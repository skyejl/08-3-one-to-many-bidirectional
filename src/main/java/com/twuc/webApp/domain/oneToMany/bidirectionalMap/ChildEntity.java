package com.twuc.webApp.domain.oneToMany.bidirectionalMap;

import javax.persistence.*;

// TODO
//
// 请使用双向映射定义 ParentEntity 和 ChildEntity 的 one-to-many 关系。其中 ChildEntity
// 的数据表应当为如下的结构。
//
// child_entity
// +───────────────────+──────────────+──────────────────────────────+
// | Column            | Type         | Additional                   |
// +───────────────────+──────────────+──────────────────────────────+
// | id                | bigint       | primary key, auto_increment  |
// | name              | varchar(20)  | not null                     |
// | parent_entity_id  | bigint       | null                         |
// +───────────────────+──────────────+──────────────────────────────+
//
// <-start-
@Entity
public class ChildEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 20,nullable = false)
    private String name;
    @ManyToOne
    @JoinColumn
    private ParentEntity parentEntity;

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParentEntity(ParentEntity parentEntity) {
        this.parentEntity = parentEntity;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ParentEntity getParentEntity() {
        return parentEntity;
    }

    public ChildEntity(String name) {
        this.name = name;
    }

    public ChildEntity(String name, ParentEntity parentEntity) {
        this.name = name;
        this.parentEntity = parentEntity;
    }

    public ChildEntity() {
    }
}
// --end->
